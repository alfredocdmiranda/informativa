Sistema da livraria Informativa
Versão atual: 1.0.3

Features:
- Cadastro de clientes
- Cadastro de três telefones por clientes
- Cadastro de editoras
- Cadastro de livros
- Cadastro de pedidos
- Emissão de relatório pdf ao salvar um pedido
- Emissão de relatórios pdf e xlxs por editora
- A janela é redimensionada de acordo com o painel
- A janela não pode ser diminuída para um tamanho menor que o painel
- As barras de rolagens foram retiradas
- Foi retirado as listas de cliente, livro e pedido na parte de adicionar e
  editor pedidos. Foram adicionados botões que abrem uma nova janela para
  selecionar o pedido.

Tutorial:
- Na aba "Cliente" é possível cadastrar um novo cliente em "Adicionar Cliente" e 
  verificar as informações dos clientes e editá-las em "Editar Cliente";

- Na aba "Livro" é possível cadastrar uma nova editora em "Adicionar Editora" e
  verificar as informações e editá-las em "Editar Editora";

- Na aba "Livro" também é possível cadastrar novos livros em "Adicionar Livro" e
  verificar as informações e editá-las em "Editar Livro";
 
- Na aba "Pedido" é possível criar novos pedidos em "Adicionar Pedido" e 
  verificar as informações sobre os pedidos e editá-las em "Editar Pedido", lá
  é possível filtrar os pedidos por status, data, nome ou cpf do cliente.
  Ao criar um pedido será gerado um pdf com as informações do pedido e pode-se
  gerar o mesmo relatório na aba para editar o pedido.

- Na aba "Pedido" é possível gerar alguns relatórios pdf em "Relatórios".

Atualmente são gerados os seguintes relatórios:
- Relatório com livros e quantidades para serem pedidos na editora e uma outra
  página com quais clientes fizeram os pedidos em ordem por data.

Bugs:
- Está ocorrendo "Segmentation Fault" quando fecha a aplicação.
