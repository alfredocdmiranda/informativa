# -*- coding: utf-8 -*-
__author__ = 'alfredo'

from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
import xlsxwriter


def report_order_editor(filename, editor_name, clients, books):
    c = canvas.Canvas(filename, pagesize=A4)

    ###TIMBRE###
    c.drawInlineImage("img/logo_t.jpg", 175, 400, 200, 150)
    c.setLineWidth(.7)
    c.setFont('Helvetica', 16)
    c.drawString(400, 810, 'B. A. Pinheiro da Silva')
    c.line(390, 800, 570, 800)

    c.setFont('Helvetica', 8)
    c.drawString(395, 790, 'CNPJ 03.528.327/0001-83 – CGF 06.289.631-8')
    c.drawString(405, 780, 'Galeria Gustavo Correia Lima, 13 – Centro')
    c.drawString(445, 770, 'Fone – (88) 3581 1040')
    c.drawString(460, 760, 'Iguatu - Ceará')
    ############

    c.setLineWidth(.3)
    c.setFont('Helvetica', 12)

    c.drawString(30, 780, 'Livraria Informativa')
    c.drawString(30, 750, 'Relatório de pedido para editora {0}'.format(editor_name.upper()))
    c.line(0, 747, 580, 747)

    c.drawString(30, 730, 'Nome do Livro')
    c.drawString(500, 730, 'Quantidade')

    line = 700
    for b in books:
        if line < 50:
            c.showPage()
            line = 750
        c.drawString(30, line, b[0])
        c.drawString(500, line, str(b[1]))
        line -= 30

    c.showPage()

    ###TIMBRE###
    c.drawInlineImage("img/logo_t.jpg", 175, 400, 200, 150)
    c.setLineWidth(.7)
    c.setFont('Helvetica', 16)
    c.drawString(400, 810, 'B. A. Pinheiro da Silva')
    c.line(390, 800, 570, 800)

    c.setFont('Helvetica', 8)
    c.drawString(395, 790, 'CNPJ 03.528.327/0001-83 – CGF 06.289.631-8')
    c.drawString(405, 780, 'Galeria Gustavo Correia Lima, 13 – Centro')
    c.drawString(445, 770, 'Fone – (88) 3581 1040')
    c.drawString(460, 760, 'Iguatu - Ceará')
    ############

    c.setLineWidth(.3)
    c.setFont('Helvetica', 12)

    c.drawString(30, 780, 'Livraria Informativa')
    c.drawString(30, 750, 'Relatório de pedido dos clientes para editora {0}'.format(editor_name.upper()))

    c.line(0, 747, 600, 747)

    line = 700

    for client in clients:
        if line < 50:
            c.showPage()
            line = 750
        c.setFont('Helvetica', 20)
        c.drawString(30, line, client)
        c.setFont('Helvetica', 12)

        c.line(30, line-10, 300, line-10)

        c.drawString(30, line-30, 'Nome do Cliente')
        c.drawString(250, line-30, 'Telefone')
        c.drawString(400, line-30, 'Data do pedido')

        line_count = 60
        for l in clients[client]:
            if line-line_count < 50:
                c.showPage()
                line = 750
                line_count = 0
            c.drawString(30, line-line_count, l[0][0:32])
            c.drawString(250, line-line_count, l[1])
            c.drawString(400, line-line_count, l[2])
            line_count += 30

        line = line - line_count - 20

    c.save()


def report_order_editor_xlsx(filename, editor_name, clients, books):
    workbook = xlsxwriter.Workbook(filename)
    report_editor = workbook.add_worksheet()
    report_editor.set_column('A:A', 20)
    bold = workbook.add_format({'bold': 1})
    report_editor.write(0, 0, u'Relatório de pedido para editora {0}'.format(editor_name.upper()), bold)

    report_editor.write(2, 0, u'Nome do Livro', bold)
    report_editor.write(2, 1, u'Autor', bold)
    report_editor.write(2, 2, u'Quantidade', bold)


    start = 3
    for index, b in enumerate(books):
        report_editor.write(start+index, 0, b[0])
        report_editor.write(start+index, 1, b[2])
        report_editor.write(start+index, 2, str(b[1]))

    report_client = workbook.add_worksheet()
    report_client.write(0, 0, u'Relatório de pedido dos clientes para editora {0}'.format(editor_name.upper()), bold)

    report_client.write(2, 1, u'Nome do Cliente', bold)
    report_client.write(2, 2, u'Telefone', bold)
    report_client.write(2, 3, u'Data do Pedido', bold)

    start = 3
    for index, client in enumerate(clients):
        pos = start + 1
        report_client.write(start, 0, unicode(client), bold)
        for index_2, l in enumerate(clients[client]):
            report_client.write(pos+index_2, 1, unicode(l[0]))
            report_client.write(pos+index_2, 2, unicode(l[1]))
            report_client.write(pos+index_2, 3, unicode(l[2]))
        start += index_2 + 3


    workbook.close()


def report_order(filename, order, orders_book, client, telephone):
    c = canvas.Canvas(filename, pagesize=A4)

    for i in range(2):
        ###TIMBRE###
        c.drawInlineImage("img/logo_t.jpg", 175, 400, 200, 150)
        c.setLineWidth(.7)
        c.setFont('Helvetica', 16)
        c.drawString(400, 810, 'B. A. Pinheiro da Silva')
        c.line(390, 800, 570, 800)

        c.setFont('Helvetica', 8)
        c.drawString(395, 790, 'CNPJ 03.528.327/0001-83 – CGF 06.289.631-8')
        c.drawString(405, 780, 'Galeria Gustavo Correia Lima, 13 – Centro')
        c.drawString(445, 770, 'Fone – (88) 3581 1040')
        c.drawString(460, 760, 'Iguatu - Ceará')
        ############

        c.setLineWidth(.3)
        c.setFont('Helvetica', 12)

        c.drawString(30, 780, u'Livraria Informativa')
        c.drawString(30, 720, u'Pedido: {0}'.format(order.code))
        c.drawString(440, 720, u'Data: {0}'.format(order.date_start))
        c.drawString(30, 690, u'Cliente: {0}'.format(client.name))
        c.drawString(30, 660, u'Endereço: {0}'.format(client.address))
        c.drawString(30, 630, u'Telefone: {0}'.format(telephone))
        c.line(0, 620, 580, 620)

        c.drawString(30, 600, u'Nome do Livro')
        c.drawString(200, 600, u'Autor')
        c.drawString(360, 600, u'Editora')
        c.drawString(460, 600, u'Preço')
        c.drawString(520, 600, u'Status')

        line = 580
        soma = 0.0
        for b in orders_book:
            if line < 50:
                c.showPage()
                line = 750
            c.drawString(30, line, unicode(b[0][0:20]))
            c.drawString(200, line, unicode(b[1][0:18]))
            c.drawString(360, line, unicode(b[2][0:10]))
            c.drawString(460, line, u"R$ "+str(b[3]))
            c.drawString(520, line, unicode(b[4]))
            soma += float(b[3])
            line -= 30

        if line < 50:
            c.showPage()
            line = 750
        c.drawString(30, line, u"TOTAL: ")
        c.drawString(80, line, u"R$ "+str(soma))
        line -= 30
        if line < 50:
            c.showPage()
            line = 750
        c.drawString(30, line, u"Forma de pagamento: ")
        c.drawString(155, line, u"{0}".format(order.payment))
        line -= 30
        if line < 50:
            c.showPage()
            line = 750
        c.drawString(30, line, u"Observação: ")
        for l in order.description.split("\n"):
            if line < 50:
                c.showPage()
                line = 750
            c.drawString(100, line, unicode(l))
            line -= 30

        c.line(0, 30, 580, 30)
        c.setFont('Helvetica', 10)
        if i == 0:
            c.drawString(30, 15, u"Via do cliente.")
        elif i == 1:
            c.drawString(30, 15, u"Via da empresa.")

        c.showPage()

    c.save()
