# -*- coding: utf-8 -*-
__author__ = 'Alfredo Miranda <alfredocdmiranda@gmail.com>'
__description__ = 'Arquivo principal do programa gerenciador do Tiago.'
__version__ = '1.0.3'
__date__ = '14/01/14'

import wx
import sqlite3
import panels

conn = sqlite3.connect("informativa.db")
cur = conn.cursor()


def init_database():
    cur.execute(u"PRAGMA foreign_keys = ON;")
    cur.execute(u"PRAGMA encoding = 'UTF-8';")

    try:
        query = u"CREATE TABLE client " \
                u"(client_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, " \
                u"cpf TEXT UNIQUE, address TEXT NULL, description TEXT NULL);"
        cur.execute(query)
    except sqlite3.OperationalError as e:
        print e
        pass

    try:
        query = u"CREATE TABLE telephone " \
                u"(telephone_id INTEGER PRIMARY KEY AUTOINCREMENT, telephone TEXT, " \
                u"tipo TEXT, client_id INTEGER, FOREIGN KEY(client_id) REFERENCES client(client_id));"
        cur.execute(query)
    except sqlite3.OperationalError as e:
        print e
        pass

    try:
        query = u"CREATE TABLE editor (editor_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, telephone TEXT NULL, " \
                u"email TEXT NULL, address TEXT NULL);"
        cur.execute(query)
    except sqlite3.OperationalError as e:
        print e
        pass

    try:
        query = u"CREATE TABLE book (book_id INTEGER PRIMARY KEY AUTOINCREMENT, codigo TEXT, " \
                u"name TEXT, author TEXT, editor_id INTEGER, price REAL, description TEXT NULL, " \
                u"FOREIGN KEY(editor_id) REFERENCES editor(editor_id));"
        cur.execute(query)
    except sqlite3.OperationalError as e:
        print e
        pass

    try:
        query = u"CREATE TABLE orders (order_id INTEGER PRIMARY KEY AUTOINCREMENT, date_start TEXT, status INTEGER, " \
                u"date_finish TEXT, client_id INTEGER, payment TEXT, description TEXT, " \
                u"FOREIGN KEY(client_id) REFERENCES client(client_id));"
        cur.execute(query)
    except sqlite3.OperationalError as e:
        print e
        pass

    try:
        query = u"CREATE TABLE order_book (order_book_id INTEGER PRIMARY KEY AUTOINCREMENT, " \
                u"order_id INTEGER, book_id INTEGER, price REAL, status INTEGER, " \
                u"FOREIGN KEY(order_id) REFERENCES orders(order_id) ON DELETE CASCADE, " \
                u"FOREIGN KEY(book_id) REFERENCES book(book_id));"
        cur.execute(query)
    except sqlite3.OperationalError as e:
        print e
        pass

    conn.commit()


class MyFrame(wx.Frame):
    """
    Classe do Frame principal da GUI.
    """

    def __init__(self, *args, **kwargs):
        super(MyFrame, self).__init__(*args, **kwargs)
        self.panels = []
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.panels.append(panels.ClientPanel(self, 'add_client', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.ClientPanel(self, 'edit_client', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.BookPanel(self, 'add_book', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.BookPanel(self, 'edit_book', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.EditorPanel(self, 'add_editor', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.EditorPanel(self, 'edit_editor', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.OrderPanel(self, 'add_order', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.OrderPanel(self, 'edit_order', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)
        self.panels.append(panels.OrderPanel(self, 'report_order', conn, cur))
        self.sizer.Add(self.panels[-1], 1, wx.EXPAND)

        for p in self.panels:
            p.Show(False)
        self.init_ui()
        self.Centre()
        self.Show(True)

    def init_ui(self):
        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        client_menu = wx.Menu()
        order_menu = wx.Menu()
        book_menu = wx.Menu()

        citem = client_menu.Append(0, u'Adicionar cliente', u'Adicionar clientes')
        self.Bind(wx.EVT_MENU, self.add_client, citem)
        citem = client_menu.Append(1, u'Editar cliente', u'Editar clientes')
        self.Bind(wx.EVT_MENU, self.edit_client, citem)

        oitem = order_menu.Append(2, u'Adicionar pedido', u'Adicionar perdidos')
        self.Bind(wx.EVT_MENU, self.add_order, oitem)
        oitem = order_menu.Append(3, u'Editar pedido', u'Editar perdidos')
        self.Bind(wx.EVT_MENU, self.edit_order, oitem)
        oitem = order_menu.Append(8, u'Relatórios', u'Gera relatórios')
        self.Bind(wx.EVT_MENU, self.report_order, oitem)

        bitem = book_menu.Append(4, u'Adicionar livro', u'Adicionar livros')
        self.Bind(wx.EVT_MENU, self.add_book, bitem)
        bitem = book_menu.Append(5, u'Editar livro', u'Editar e listar livros')
        self.Bind(wx.EVT_MENU, self.edit_book, bitem)

        bitem = book_menu.Append(6, u'Adicionar editora', u'Adicionar editoras')
        self.Bind(wx.EVT_MENU, self.add_editor, bitem)
        bitem = book_menu.Append(7, u'Editar editora', u'Editar editoras')
        self.Bind(wx.EVT_MENU, self.edit_editor, bitem)

        fitem = file_menu.Append(wx.ID_EXIT, u'Quit', u'Quit application')
        self.Bind(wx.EVT_MENU, self.close, fitem)
        self.Bind(wx.EVT_CLOSE, self.close)

        menubar.Append(file_menu, '&File')
        menubar.Append(client_menu, '&Cliente')
        menubar.Append(order_menu, '&Pedido')
        menubar.Append(book_menu, '&Livro')
        self.SetMenuBar(menubar)

    def add_client(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'add_client':
                self.resize(p)
                p.Show(True)

    def edit_client(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'edit_client':
                self.resize(p)
                p.Show(True)
                p.update_list()

    def add_book(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'add_book':
                self.resize(p)
                p.Show(True)
                p.update_combo()

    def edit_book(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'edit_book':
                self.resize(p)
                p.Show(True)
                p.update_list()
                p.update_combo()

    def add_editor(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'add_editor':
                self.resize(p)
                p.Show(True)

    def edit_editor(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'edit_editor':
                self.resize(p)
                p.Show(True)
                p.update_list()

    def add_order(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'add_order':
                self.resize(p)
                p.Show(True)
                #p.update_list_client()
                #p.update_list_book()

    def edit_order(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'edit_order':
                self.resize(p)
                p.Show(True)
                #p.update_list_order()
                #p.update_list_book()

    def report_order(self, event):
        for p in self.panels:
            p.Show(False)
            if p.func == 'report_order':
                self.resize(p)
                p.Show(True)
                p.update_combo()

    def close(self, event):
        #exit(1)
        for child in self.GetChildren():
            child.Destroy()
        self.Destroy()

    def resize(self, p):
        p_weight, p_height = p.GetSize()
        d_weight, d_height = wx.DisplaySize()

        if d_weight <= p_weight or d_height <= p_height:
            self.Maximize()
            self.SetMinSize((d_weight, d_height))
        else:
            self.SetMinSize((p_weight, p_height))

        if not self.IsMaximized():
            self.SetSize((weight, height))

if __name__ == '__main__':
    init_database()
    app = wx.App(False)
    weight, height = wx.DisplaySize()
    if weight >= 800 and height >= 600:
        frame = MyFrame(parent=None, style=wx.DEFAULT_FRAME_STYLE,
                        title=u"Gerenciador de Pedidos")
        frame.Maximize()
    else:
        msg = u"Você deve usar uma resolução de no mínimo 800x600."
        wx.MessageBox(msg, u'Info', wx.OK | wx.ICON_ERROR)
    app.MainLoop()