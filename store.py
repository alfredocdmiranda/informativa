# -*- coding: utf-8 -*-
__author__ = 'Alfredo Miranda <alfredocdmiranda@gmail.com>'
__description__ = 'Arquivo de classes.'
__version__ = '0.1'
__date__ = '22/10/13'

import string
import re


class Client(object):
    def __init__(self, name, cpf, address=None, description=None, code=None):
        self.code = code
        self.name = name.upper()
        if self.cpf_validation(cpf):
            self.cpf = cpf
        else:
            raise ValueError(u"CPF inválido.")
        self.address = unicode(address).upper()
        self.description = unicode(description).upper()

    def save(self, cur, conn):
        if not self.code:
            query = u"INSERT INTO client (name, cpf, address, description) " \
                    u"VALUES ('{0}', '{1}', '{2}', '{3}');"\
                    .format(self.name, self.cpf, self.address, self.description)
            cur.execute(query)
            conn.commit()
            query = u"SELECT MAX(client_id) FROM client;"
            cur.execute(query)
            self.code = cur.fetchone()[0]
        else:
            query = u"UPDATE client SET name='{0}',cpf='{1}',address='{2}',description='{3}' WHERE client_id={4};"\
                    .format(self.name, self.cpf, self.address, self.description, self.code)
            cur.execute(query)
            conn.commit()

    @classmethod
    def load(cls, cur, code=None, cpf=None):
        if code:
            query = u"SELECT * FROM client WHERE client_id={0}".format(code)
        elif cpf:
            query = u"SELECT * FROM client WHERE cpf='{0}'".format(cpf)
        cur.execute(query)

        r = cur.fetchall()[0]

        return Client(r[1], r[2], r[3], r[4], r[0])

    @staticmethod
    def cpf_validation(cpf):
        if len(cpf) != 11:
            return False
        init_cpf = cpf[0:9]

        count = 10
        soma = 0
        for i in init_cpf:
            soma += count * int(i)
            count -= 1


        resto = soma % 11
        if resto < 2:
            digit_1 = str(0)
        else:
            digit_1 = str(11 - resto)

        if digit_1 != cpf[-2]:
            return False

        init_cpf += digit_1

        count = 11
        soma = 0
        for i in init_cpf:
            soma += count * int(i)
            count -= 1

        resto = soma % 11
        if resto < 2:
            digit_2 = str(0)
        else:
            digit_2 = str(11 - resto)

        if digit_2 != cpf[-1]:
            return False

        return True


class Telephone(object):
    def __init__(self, telephone, tipo, client_id=None, code=None):
        self.code = code

        if self.tel_validation(telephone):
            self.telephone = telephone
        else:
            raise ValueError(u"Telefone inválido.")
        self.tipo = tipo
        self.client_id = client_id

    def save(self, cur, conn):
        if not self.code:
            query = u"INSERT INTO telephone (telephone, tipo, client_id) " \
                    u"VALUES ('{0}', '{1}', {2});".format(self.telephone, self.tipo, self.client_id)
            cur.execute(query)
            conn.commit()
            query = u"SELECT MAX(telephone_id) FROM telephone;"
            cur.execute(query)
            self.code = cur.fetchone()[0]
        else:
            query = u"UPDATE telephone SET telephone='{0}',tipo='{1}',client_id='{2}' WHERE telephone_id={3};"\
                    .format(self.telephone, self.tipo, self.client_id, self.code)
            cur.execute(query)
            conn.commit()

    @classmethod
    def load(cls, cur, code):
        query = u"SELECT * FROM telephone WHERE telephone_id={0}".format(code)
        cur.execute(query)
        r = cur.fetchall()[0]

        return Telephone(r[1], r[2], r[3], r[0])

    @classmethod
    def load_all(cls, cur, code):
        query = u"SELECT * FROM telephone WHERE client_id={0}".format(code)
        cur.execute(query)
        result = cur.fetchall()
        ls = []
        for r in result:
            ls.append(Telephone(r[1], r[2], r[3], r[0]))

        return ls

    @staticmethod
    def tel_validation(tel):
        for t in tel:
            if t in string.ascii_letters:
                return False
        return True


class Book(object):
    def __init__(self, name, price, author, editor_id, description=None, code_editor=None, code=None):
        self.code = code
        self.code_editor = code_editor
        self.name = name.upper()
        self.price = price
        self.author = author.upper()
        self.editor_id = editor_id
        self.description = unicode(description).upper()

    def save(self, cur, conn):
        if not self.code:
            query = u"INSERT INTO book (name, author, editor_id, price, description, codigo) " \
                    u"VALUES ('{0}', '{1}', {2}, {3}, '{4}', '{5}');"\
                    .format(self.name, self.author, self.editor_id, self.price, self.description, self.code_editor)
            cur.execute(query)
            conn.commit()
            query = u"SELECT MAX(book_id) FROM book;"
            cur.execute(query)
            self.code = cur.fetchone()[0]
        else:
            query = u"UPDATE book SET name='{0}',author='{1}',editor_id={2},price={3},description='{5}',codigo='{6}' " \
                    u"WHERE book_id={4};"\
                    .format(self.name, self.author, self.editor_id, self.price, self.code, self.description,
                            self.code_editor)
            cur.execute(query)
            conn.commit()

    @classmethod
    def load(cls, cur, name, author=None, editor=None):
        query = u"SELECT name, price, author, editor_id, description, codigo, book_id" \
                u" FROM book WHERE name='{0}' AND author='{1}';".format(name, author)
        cur.execute(query)
        r = cur.fetchone()
        return cls(r[0], r[1], r[2], r[3], r[4], r[5], r[6])


class Editor(object):
    def __init__(self, name, telephone=None, email=None, address=None, code=None):
        self.code = code
        self.name = name.upper()
        if self.tel_validation(telephone):
            self.telephone = telephone
        else:
            raise ValueError(u"Telefone Inválido")
        if self.email_validation(email):
            self.email = unicode(email).upper()
        else:
            raise ValueError(u"Email Inválido")
        self.address = unicode(address).upper()

    def save(self, cur, conn):
        if not self.code:
            query = u"INSERT INTO editor (name, telephone, email, address) " \
                    u"VALUES ('{0}', '{1}', '{2}', '{3}');".format(self.name, self.telephone, self.email, self.address)
            cur.execute(query)
            conn.commit()
            query = u"SELECT MAX(editor_id) FROM editor;"
            cur.execute(query)
            self.code = cur.fetchone()[0]
        else:
            query = u"UPDATE editor SET name='{0}' WHERE editor_id={1};".format(self.name, self.code)
            cur.execute(query)
            conn.commit()

    @staticmethod
    def tel_validation(tel):
        if tel is not tel:
            for t in tel:
                if t in string.ascii_letters:
                    return False
        return True

    @staticmethod
    def email_validation(email):
        if email is not None:
            if not re.match(r"[^@]+@[^@]+\.[^@]+", email) and not len(email) == 0:
                return False
            else:
                return True
        else:
            return True


class Order(object):
    def __init__(self, client_id=None, date_start=None, date_finish=None, status=None, payment=None,
                 description=None, code=None):
        self.code = code
        self.client_id = client_id
        self.date_start = date_start
        self.date_finish = date_finish
        self.status = status
        self.payment = unicode(payment).upper()
        self.description = unicode(description).upper()

    def save(self, cur, conn):
        if not self.code:
            query = u"INSERT INTO orders (client_id, date_start, date_finish, status, payment, description) " \
                    u"VALUES ({0}, '{1}', '{2}', {3}, '{4}', '{5}');"\
                    .format(self.client_id, self.date_start, self.date_finish, self.status, self.payment,
                            self.description)
            cur.execute(query)
            conn.commit()
            query = u"SELECT MAX(order_id) FROM orders;"
            cur.execute(query)
            self.code = cur.fetchone()[0]
        else:
            query = u"UPDATE orders SET client_id={0},date_start='{1}',status={2},payment='{3}',date_finish='{5}'," \
                    u"description='{6}' WHERE order_id={4};"\
                    .format(self.client_id, self.date_start, self.status, self.payment, self.code, self.date_finish,
                            self.description)
            cur.execute(query)
            conn.commit()

    def delete(self, cur, conn):
        query = u"DELETE FROM order_book WHERE order_id={0};".format(self.code)
        cur.execute(query)
        query = u"DELETE FROM orders WHERE order_id={0};".format(self.code)
        cur.execute(query)
        conn.commit()

    @classmethod
    def load(cls, cur, cpf, code):
        query = u"SELECT orders.order_id,client.client_id,orders.date_start,orders.date_finish,orders.status," \
                u"orders.payment, orders.description FROM orders JOIN client ON orders.client_id=client.client_id " \
                u"WHERE client.cpf='{0}' AND orders.order_id={1};".format(cpf, code)
        cur.execute(query)
        r = cur.fetchall()[0]
        return Order(r[1], r[2], r[3], r[4], r[5], r[6], r[0])


class OrderBook(object):
    def __init__(self, order_id=None, book_id=None, price=None, status=None, code=None):
        self.code = code
        self.order_id = order_id
        self.book_id = book_id
        self.price = price
        self.status = status

    def save(self, cur, conn):
        if not self.code:
            query = u"INSERT INTO order_book (order_id, book_id, price, status) VALUES ({0}, {1}, {2}, {3});"\
                    .format(self.order_id, self.book_id, self.price, self.status)
            cur.execute(query)
            conn.commit()
            query = u"SELECT MAX(order_book_id) FROM order_book;"
            cur.execute(query)
            self.code = cur.fetchone()[0]
        else:
            query = u"UPDATE order_book SET order_id={0},book_id={1},price={2},status={3} WHERE order_book_id={4};"\
                    .format(self.order_id, self.book_id, self.price, self.status, self.code)
            cur.execute(query)
            conn.commit()

    def delete(self, cur, conn):
        query = u"DELETE FROM order_book WHERE order_book_id={0};".format(self.code)
        cur.execute(query)
        conn.commit()

    @classmethod
    def load(cls, cur, code):
        query = u"SELECT * FROM order_book WHERE order_book_id={0}".format(code)
        cur.execute(query)
        r = cur.fetchall()[0]

        return OrderBook(r[1], r[2], r[3], r[4], r[0])

    @classmethod
    def load_all(cls, cur, order_code):
        query = u"SELECT * FROM order_book WHERE order_id={0}".format(order_code)
        cur.execute(query)
        ls = []
        result = cur.fetchall()
        for r in result:
            ls.append(OrderBook(r[1], r[2], r[3], r[4], r[0]))

        return ls
